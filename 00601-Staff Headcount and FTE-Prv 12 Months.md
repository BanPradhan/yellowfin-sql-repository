# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ### To re-create reports where necessary
SELECT DISTINCT
   "D_DATE"."MONTH_YEAR_MM",
   "D_HR_EMPLOYEE_STATUS"."TENURE_TYPE_GROUP_CD",
   SUM("F_HR_MONTHLY_FTE"."FTE"),
   COUNT(DISTINCT("F_HR_MONTHLY_FTE"."EMPLOYEE_NUMBER"))
FROM "STAFFMART"."F_HR_MONTHLY_FTE"
INNER JOIN "STAFFMART"."D_DATE"
ON (
   "F_HR_MONTHLY_FTE"."SNAPSHOT_DATE_PK" = "D_DATE"."DATE_PK"
)
INNER JOIN "STAFFMART"."D_HR_EMPLOYEE_STATUS"
ON (
   "F_HR_MONTHLY_FTE"."EMPLOYEE_STATUS_PK" = "D_HR_EMPLOYEE_STATUS"."EMPLOYEE_STATUS_PK"
)
WHERE (
   (
      "D_DATE"."PREVIOUS_12_MONTHS_IND" = 'Y'
   )
)
GROUP BY 
   "D_DATE"."MONTH_YEAR_MM",
   "D_HR_EMPLOYEE_STATUS"."TENURE_TYPE_GROUP_CD"
* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact